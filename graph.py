import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
import numpy as np
import math

df_movie_comment = pd.read_csv('df_movie_review.csv')

top10 = df_movie_comment.title.value_counts().sort_values(ascending=False)[:10]
top10_title = top10.index.tolist()
top10_reviews = df_movie_comment[df_movie_comment['title'].isin(top10_title)]

#-- 평균 평점 계산
movie_title = top10_reviews.title.unique().tolist()    #-- 영화 제목 추출
avg_score = {}  #-- {제목 : 평균} 저장
for t in movie_title:
    avg = top10_reviews[top10_reviews['title'] == t]['score'].mean()
    avg_score[t] = avg

plt.rc('font', family='NanumGothic')
    
plt.figure(figsize=(10, 5))
plt.title('영화 평균 평점', fontsize=15)
#plt.xlabel('Title')
#plt.ylabel('Average rating')
plt.xticks(rotation=30)

for x, y in avg_score.items():
    color = np.array_str(np.where(y == max(avg_score.values()), 'orange', 'lightgrey'))
    plt.bar(x, y, color=color)
    plt.text(x, y, '%.2f' % y, 
             horizontalalignment='center',  
             verticalalignment='bottom')    

plt.tight_layout()
plt.savefig('1.jpg')

fig, axs = plt.subplots(5, 2, figsize=(15, 25))
axs = axs.flatten()

for title, avg, ax in zip(avg_score.keys(), avg_score.values(), axs):    
    num_reviews = len(top10_reviews[top10_reviews['title'] == title])   
    x = np.arange(num_reviews)
    y = top10_reviews[top10_reviews['title'] == title]['score']
    ax.set_title('\n%s (%d명)' % (title, num_reviews) , fontsize=15)
    ax.set_ylim(0, 10.5, 2) 
    ax.plot(x, y, 'o')    
    ax.axhline(avg, color='red', linestyle='--') #-- 평균 점선 나타내기    
    
#plt.show()
plt.tight_layout()
plt.savefig('2.jpg')
