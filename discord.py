# https://github.com/kyb3r/dhooks
from dhooks import Webhook, File
from io import BytesIO
from datetime import datetime
from pytz import timezone
import requests
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('arg1')
args = parser.parse_args()

hook = Webhook(args.arg1)

now = datetime.now(timezone('Asia/Seoul')).strftime('%Y-%m-%d_%H%M%S')

file = File('df_movie_review.csv', name='df_movie_review_{0}.csv'.format(now))
file2 = File('1.jpg', name='1.jpg')
file3 = File('2.jpg', name='2.jpg')
hook.send(file=file)
hook.send(file=file2)
hook.send(file=file3)
