#!/usr/bin/env python
# coding: utf-8

from bs4 import BeautifulSoup
from urllib.request import urlopen
import pandas as pd
import numpy as np
import re
from pykospacing import Spacing

# 네이버 영화 댓글 수집
# 영화제목, 평점, 댓글 수집
url='https://movie.naver.com/movie/point/af/list.naver?&page=1'
html = urlopen(url)
soup = BeautifulSoup(html, 'lxml')
#soup.select('td.title a.movie.color_b')
# 평점 수집
#soup.select('td.title div em')
#soup.select('td.title')[0].select_one('a.report')
# 원본
#soup.select('td.title')[0].select_one('a.report')['onclick'].split(',')[2]
# 개선본
#soup.select('td.title')[0].select_one('a.report')['onclick'].split('\'')[5]

#get_ipython().run_cell_magic('capture', '', 'for num in range(1,1001) :\n  new_url = url[:-1]+str(num)\n  print(new_url)\n')

titles = []
stars = []
comments = []
new_url = url+str(1)
html = urlopen(new_url)
soup = BeautifulSoup(html, 'lxml')
for i in soup.select('td.title') :
  title = i.select_one('a.movie.color_b').text
  star = i.select_one('em').text
  # comment = i.select_one('a.report')['onclick'].split(',')[2]
  comment = i.select_one('br').next_sibling.strip()
  titles.append(title)
  stars.append(star)
  comments.append(comment)


# 댓글, 평점, 영화제목 크롤링 함수
def page_crawling(page_num) :
  titles = []
  stars = []
  comments = []
  url='https://movie.naver.com/movie/point/af/list.naver?&page='
  new_url = url+str(page_num)
  html = urlopen(new_url)
  soup = BeautifulSoup(html, 'lxml')
  for i in soup.select('td.title') :
    title = i.select_one('a.movie.color_b').text
    star = i.select_one('em').text
    # comment = i.select_one('a.report')['onclick'].split(',')[2]
    comment = i.select_one('br').next_sibling.strip() # 바로 다음 텍스트 추출
    titles.append(title)
    stars.append(star)
    comments.append(comment)
  return titles, stars, comments



fin_title = []
fin_star = []
fin_comment = []
for num in range(1,1001) :
  titles, stars, comment = page_crawling(num)
  fin_title.extend(titles)
  fin_star.extend(stars)
  fin_comment.extend(comment)
#print(len(fin_title))
#print(len(fin_star))
#print(len(fin_comment))

df_movie_comment = pd.DataFrame(zip(fin_title, fin_star, fin_comment))
#print(df_movie_comment.head())

# 컬럼명 설정
df_movie_comment.columns = ['title', 'score', 'review']
#df_movie_comment
#print(df_movie_comment.isnull().sum())

# 한글 표현만 남기기(숫자, 특수문자, 영어 제거)
def extract_word(text):
    hangul = re.compile('[^가-힣]') 
    result = hangul.sub(' ', text) 
    return result


# 특정 리뷰를 통해 전과 후를 비교해보자
#print("Before Extraction : ",df_movie_comment['review'][3])
#print("After Extraction : ", extract_word(df_movie_comment['review'][3]))
#print("Before Extraction : ", df_movie_comment['review'][10])
#print("After Extraction : ", extract_word(df_movie_comment['review'][10]))
#print("Before Extraction : ",df_movie_comment['review'][0])
#print("After Extraction : ", extract_word(df_movie_comment['review'][0]))

df_movie_comment['review'] = df_movie_comment['review'].apply(lambda x:extract_word(x))
#df_movie_comment['review']

# 띄어쓰기 처리
spacing = Spacing()
df_movie_comment['review'] = df_movie_comment['review'].apply(lambda x:spacing(x))

# csv export (to_csv)
df_movie_comment.to_csv('df_movie_review.csv', encoding='utf-8-sig', index=False)
